module.exports = {
  mongoDb: {
    dbString: process.env.DB_STRING
  },
  s_key: {
    gingerbread: process.env.KEY_GINGERBREAD,
    Oatmeal: process.env.KEY_OATMEAL,
    Shortbread: process.env.KEY_SHORTBREAD,
    peanut: process.env.KEY_PEANUT
  },
  mail_key: {
    email_address: "rokki-inc@yandex.com",
    pass: "@2k19all41"
  },
  super_key: {
    naruto: process.env.VAL_NARUTO,
    sasuke: process.env.VAL_SASUKE,
    yagami: process.env.VAL_YAGAMI,
    kakashi: process.env.VAL_KAKASHI,
    lelouch: process.env.VAL_LELOUCH,
    minata: process.env.VAL_MINATA,
    jiraiya: process.env.VAL_JIRAIYA,
    luffy: process.env.VAL_LUFFY
  }
};
