const express = require("express");
const router = express.Router();
const mongoose = require("mongoose");
const User = require("../../models/users");
const Stories = require("../../models/stories");

router.get("*", (req, res, next) => {
  if (req.user) {
    next();
  } else {
    req.flash("error_msg", "You Should login first");
    res.redirect("/login");
  }
});

router.get("/", async (req, res) => {
  let writer;
  let stories = await Stories.find({ published: true });
  let data = {
    stories,
    userName: req.user.userName
  };
  res.render("home/index", data);
});

router.get("/:title", async (req, res) => {
  let writer;
  let stories = await Stories.find({
    published: true,
    link: req.params.title.toLowerCase()
  });
  if (stories) {
    console.log(stories);
    let data = {
      stories,
      userName: req.user.userName
    };
    res.render("home/index", data);
  } else {
    res.redirect("/404-story");
  }
});

module.exports = router;
