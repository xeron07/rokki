const express = require("express");
const router = express.Router();
const passport = require("passport");

router.get("/", (req, res) => {
  if (req.user) {
    res.redirect("/story");
  }
  res.render("login/index");
});

router.post("/user", (req, res, next) => {
  const { email, password } = req.body;
  let errors = [];
  if (email === "" || password === "")
    errors.push({ msg: "Please fill up the fields" });

  if (errors.length > 0) {
    res.render("login/index", {
      errors,
      email,
      password
    });
  } else {
    passport.authenticate("local", {
      successRedirect: "/story",
      failureRedirect: "/login",
      failureFlash: true
    })(req, res, next);
  }
});

module.exports = router;
