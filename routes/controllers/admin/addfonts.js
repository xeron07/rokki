const router = require("express").Router();
const mongoose = require("mongoose");
const Font = require("../../../models/fonts");
router.get("/", async (req, res) => {
  let font = new Font({
    name: "absolute-pink",
    link: "/ext/editor/fonts/absolute-pink/Absolute-Pink.otf",
    licence: "Personal Use",
    lic_link: "/ext/editor/fonts/absolute-pink/License-Request.txt"
  });

  let ff = await font.save();

  font = new Font({
    name: "alex-brush",
    link: "/ext/editor/fonts/alex-brush/AlexBrush-Regular.ttf",
    licence: "free",
    lic_link: "/ext/editor/fonts/alex-brush/License.txt"
  });

  ff = await font.save();
  res.send("success");
});

module.exports = router;
