const mongoose = require("mongoose");
const { Schema } = mongoose;

const genreSchema = new Schema({
  g_id: String,
  g_title: String,
  t_story: { type: Number, default: 0 },
  g_img: { type: String, default: "default.jpg" },
  g_family: String,
  link: String,
  g_xt: Array
});

module.exports = mongoose.model("genre", genreSchema);
