const mongoose = require("mongoose");
const { Schema } = mongoose;

const fontSchema = new Schema({
  name: String,
  link: String,
  xtr: String,
  licence: { type: String, default: "free" },
  lic_link: { type: String, default: "no-link" }
});

module.exports = mongoose.model("fonts", fontSchema);
