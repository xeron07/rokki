const swalWithBootstrapButtons = Swal.mixin({
  customClass: {
    confirmButton: "btn btn-outline-success",
    cancelButton: "btn btn-outline-danger"
  },
  buttonsStyling: false
});

let selectStyle = a => {
  $(".select-dropdown").css({
    "border-bottom-color": "#27ae60",
    "border-bottom-size": "2px",
    color: "#f1f2f6"
  });
  $("#sTitle").css("display", "none");
};

let saveStory = async x => {
  if ($("#story-title").val() !== "" && $("#selectItems").val() != null) {
    let titleChange = false;
    console.log("ss:" + x);
    $("#test").html(val.toString());
    $("#buttons").fadeOut(1000, async () => {
      $("#loader").fadeIn();

      if ($("#story-title").val() !== s_title) {
        let decision = await swalWithBootstrapButtons.fire({
          title: "Are you sure to change the STORY TITLE ?",
          text: "Connection link will also be changed.",
          type: "warning",
          showCancelButton: true,
          confirmButtonText: "Yes, Change It",
          cancelButtonText: "No, Cancel",
          reverseButtons: true
        });
        console.log(decision);

        if (decision.value) {
          titleChange = true;
          Swal.fire({
            type: "success",
            title: "Title will change",
            showConfirmButton: false,
            timer: 1000
          });
        }
      }

      saveData(x, titleChange)
        .then(
          res => {
            if (res.status === 200) {
              showAgain("success", res.msg);
              setTimeout(() => {
                let paths = window.location.pathname.split("/");
                paths[paths.length - 1] = res.cng.link;

                if (res.cng.willCng === true) {
                  window.location.href = paths.join("/");
                }
              }, 3000);
            }
          },
          err => {
            showAgain("error", err);
          }
        )
        .catch();
    });
  } else {
    console.log("no-title");
    if ($("#story-title").val() === "") {
      $("#story-title").addClass("invalid");
      $("#story-title").prop("aria-invalid", "true");
    }
    if ($("#selectItems").val() == null) {
      console.log("in");
      $(".select-dropdown").css({
        "border-bottom-color": "#ff3838",
        "border-bottom-size": "5px"
      });
      $("#sTitle").css("display", "inline");
    }
    Swal.fire("Oops!!", "Please fill up all the fields", "error");
  }
};
let showAgain = (res, msg) => {
  //show messeage
  $("#loader").fadeOut(1000, () => {
    if (res === "success") {
      $("#s_msg").text(msg);
      $("#success-message").fadeIn(1000);
    } else {
      $("#e_msg").text(msg);
      $("#error-message").fadeIn(1000);
    }

    //Show buttons
    setTimeout(() => {
      if (res === "success") {
        $("#success-message").fadeOut(1000, () => {
          $("#buttons").fadeIn(1000);
        });
      } else {
        $("#error-message").fadeOut(1000, () => {
          $("#buttons").fadeIn(1000);
        });
      }
    }, 5000);
  });
};

//ajax request for updating

let saveData = (x, isTitleChanged) => {
  return new Promise((resolve, reject) => {
    let val = "none";
    let isPublished = false;
    let link = window.location.pathname + "/save";
    console.log(x);
    if (x === "publish") {
      console.log("why:" + (x === "publish"));
      isPublished = true;
    }

    console.log(isTitleChanged);

    //ajax request
    $.ajax({
      method: "POST",
      url: link,
      data: {
        html: quill.root.innerHTML,
        content: JSON.stringify(quill.getContents()),
        txt: quill.getText(),
        user: userName,
        title: $("#story-title").val(),
        genre: $("#selectItems").val(),
        isTitleChanged: isTitleChanged,
        isPublish: isPublished
      },
      success: res => {
        console.log("done");
        if (res.status === 200) {
          Swal.fire({
            position: "top-end",
            type: "success",
            title: "Your work has been Updated",
            showConfirmButton: false,
            timer: 2000
          });
          resolve(res);
        } else if (res.status === 400) {
          Swal.fire({
            position: "top-end",
            type: "warning",
            title: "Your work not Updated",
            showConfirmButton: false,
            timer: 2000
          });
          reject(Error(res.msg));
        } else {
          reject(Error(res.msg));
        }
      },
      error: err => {
        console.error(err);
        reject(Error("Broken"));
      }
    });
  });
};

let addContent = data => {
  //console.log(JSON.parse(data));
  quill.setContents(data);
  return "ok";
};

let blockConsole = () => {
  console.clear();
  setTimeout(blockConsole, 1000);
};
