let quill = null;
let toolbarOptions = [
  [{ font: [] }],
  [
    {
      fontSize: [
        "1px",
        "2px",
        "3px",
        "4px",
        "5px",
        "7px",
        "8px",
        "10px",
        "11px",
        "12px",
        "13px",
        "14px",
        "15px"
      ]
    }
  ], // custom dropdown
  [{ header: 1 }, { header: 2 }], // custom button values
  [{ size: ["small", false, "large", "huge"] }], // custom dropdown
  [{ align: [] }],
  [{ list: "ordered" }, { list: "bullet" }],
  [{ script: "sub" }, { script: "super" }], // superscript/subscript
  ["blockquote", "code-block"],
  [{ indent: "-1" }, { indent: "+1" }], // outdent/indent
  [{ direction: "rtl" }], // text direction
  [{ header: [1, 2, 3, 4, 5, 6, false] }],

  [{ color: [] }, { background: ["#c0392b", "#16a085", "#f39c12"] }], // dropdown with defaults from theme

  ["bold", "italic", "underline", "strike"], // toggled buttons

  ["clean"] // remove formatting button
];

//font style added
let addFonts = async arr => {
  let fArr = [];
  let style = "<style type='text/css'>";
  let a;
  for (let i = 0; i < arr.length; i++) {
    let font = new FontFace(arr[i].name, "url(" + arr[i].link + ")");

    let loaded = await font.load();
    document.fonts.add(loaded);
    fArr[i] = arr[i].name;
    console.log("fuck");
    a = " .ql-font-" + fArr[i] + "{font-family: '" + fArr[i] + "'; }";
    style += a;
    console.log(a);
  }

  if (arr.length != 0) {
    style += "</style>";
    $(style).appendTo("head");
    let Font = Quill.import("formats/font");
    addFontTools(arr);
    Font.whitelist = fArr;
    Quill.register(Font, true);
  } else {
    console.log("default fonts added");
  }

  addFontSize();
};

let addFontTools = fontsArr => {
  let str = "";
  for (let i = 0; i < fontsArr.length; i++) {
    str +=
      "<option value='" +
      fontsArr[i].name +
      "'>" +
      '<i class="ql-font-' +
      fontsArr[i].name +
      '"  >' +
      fontsArr[i].name +
      "</i></option>";
  }
  $(".ql-font").html(str);
  console.log($(".ql-font"));
};

let addFontSize = () => {
  let arr = [];
  for (let i = 1; i <= 100; i++) arr[i] = i + "px";
  var Size = Quill.import("attributors/style/size");
  Size.whitelist = arr;
  Quill.register(Size, true);
  let str = "";
  for (let i = 1; i <= 100; i++) {
    if (i == 12)
      str += "<option value=" + arr[i] + " selected>" + i + "</option>";
    else str += "<option value=" + arr[i] + ">" + i + "</option>";
  }
  $("#ql-size").html(str);
  createQuill();
};

//editor's options
let editor_options = {
  debug: "info",
  modules: {
    toolbar: "#toolbar"
  },
  placeholder: "Compose an epic...",
  theme: "snow"
};

//creating new editor's
let createQuill = () => {
  quill = new Quill("#editor", editor_options);
};

// let clearConsole=()=>{ setTimeout(()=>{console.clear(); clearConsole();}, 4000)};
